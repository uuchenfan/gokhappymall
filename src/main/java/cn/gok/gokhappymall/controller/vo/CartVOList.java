package cn.gok.gokhappymall.controller.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author: chenfan
 * @Description:
 */
@Data
public class CartVOList {

    private List<CartVO> cartProductVoList;

    private boolean allChecked;

    private String cartTotalPrice;

}
