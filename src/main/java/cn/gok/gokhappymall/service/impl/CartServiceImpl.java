package cn.gok.gokhappymall.service.impl;

import cn.gok.gokhappymall.common.ServerResponse;
import cn.gok.gokhappymall.controller.vo.CartVO;
import cn.gok.gokhappymall.controller.vo.CartVOList;
import cn.gok.gokhappymall.dao.CartMapper;
import cn.gok.gokhappymall.dao.ProductMapper;
import cn.gok.gokhappymall.entity.Cart;
import cn.gok.gokhappymall.entity.Product;
import cn.gok.gokhappymall.service.CartService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: chenfan
 * @Description:
 */
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    CartMapper cartMapper;

    @Autowired
    ProductMapper productMapper;


    @Override
    public ServerResponse<CartVOList> addCart(Cart cart) {

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", cart.getUserId());
        Integer count = cartMapper.selectCount(queryWrapper);

        //超出购物车商品添加上，则不让添加
        if (count > 30){
          return ServerResponse.createByErrorMessage("购物车添加超出上限，请删除部分商品");
        }

        //超出商品库存上限，也不让添加
        Product pro = productMapper.selectById(cart.getProductId());
        if (pro.getStock() < cart.getQuantity()){
            return ServerResponse.createByErrorMessage("超出库存上限，请修改购买数量");
        }


        //查询该商品是否已经被添加过，如果添加过，则进行购物车数量的修改，否则进行添加操作
        QueryWrapper queryWrapper1 = new QueryWrapper();
        queryWrapper1.eq("user_id",cart.getUserId());
        queryWrapper1.eq("product_id",cart.getProductId());
        Cart selectOne = cartMapper.selectOne(queryWrapper1);
        int i = 0;
        if (selectOne == null){
            //将数据插入购物车
           i = cartMapper.insert(cart);
        }else {
            //修改购物车商品数量
            i = cartMapper.updateCartProductNum(selectOne.getQuantity() + cart.getQuantity(),cart.getProductId(),cart.getUserId());
        }


        CartVOList cartVOList = new CartVOList();
        List<CartVO> cartVOs = new ArrayList<>();
        cartVOList.setCartProductVoList(cartVOs);

        //如果添加成功，则进行购物车数据的回显
        if (i > 0){
            List<Cart> cartList = cartMapper.selectList(queryWrapper);

            BigDecimal cartTotalPrice = new BigDecimal("0");

            //统计购物车已选中的商品的数量
            int checkedNum = 0;

            for (Cart cart1 : cartList) {
                Integer productId = cart1.getProductId();
                //查出商品信息
                Product product = productMapper.selectById(cart1.getProductId());

                CartVO cartVO = new CartVO();
                BeanUtils.copyProperties(product, cartVO);
                BeanUtils.copyProperties(cart1, cartVO);

                //得到当前这一种商品在购物车中的总价
                BigDecimal price = product.getPrice();
                Integer productNum = cart1.getQuantity();
                BigDecimal multiply = price.multiply(new BigDecimal(productNum));
                cartVO.setProductTotalPrice(multiply.toString());

                //将处于选中状态的商品的总金额进行累加 , 以及checkedNum+1
                if(cart1.getChecked() == 1){
                    cartTotalPrice = cartTotalPrice.add(multiply);
                    checkedNum += 1;
                }

                cartVO.setLimitQuantity("LIMIT_NUM_SUCCESS");
                cartVOs.add(cartVO);
            }


            if (checkedNum == cartList.size()){
                cartVOList.setAllChecked(true);
            }
            cartVOList.setCartTotalPrice(cartTotalPrice.toString());


            return ServerResponse.createBySuccess(cartVOList);
        }

        return ServerResponse.createBySuccessMessage("添加失败");
    }
}
