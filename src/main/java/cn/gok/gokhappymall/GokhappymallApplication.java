package cn.gok.gokhappymall;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@MapperScan("cn.gok.gokhappymall.dao")
public class GokhappymallApplication {

    public static void main(String[] args) {
        SpringApplication.run(GokhappymallApplication.class, args);
    }


}
