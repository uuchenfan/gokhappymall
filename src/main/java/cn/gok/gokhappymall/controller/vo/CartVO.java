package cn.gok.gokhappymall.controller.vo;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


import java.math.BigDecimal;

/**
 * @Author: chenfan
 * @Description:  自定义的购物详细信息展示对象
 */
@Data
public class CartVO {

    private Integer id;

    private Integer userId;

    private Integer productId;

    private Integer quantity;

    @JsonProperty("productChecked")
    private Integer checked;

    @JsonProperty("productName")
    private String name;

    @JsonProperty("productSubtitle")
    private String subtitle;

    @JsonProperty("productMainImage")
    private String mainImage;

    @JsonProperty("productPrice")
    private BigDecimal price;
    @JsonProperty("productStatus")
    private Integer status;
    @JsonProperty("productStock")
    private Integer stock;

    private String productTotalPrice;
    private String limitQuantity;

}
