package cn.gok.gokhappymall.service;


import cn.gok.gokhappymall.common.ServerResponse;
import cn.gok.gokhappymall.controller.vo.OrderVO;

import java.io.IOException;
import java.util.Map;

/**
 * @Author: chenfan
 * @Description:
 */
public interface OrderService {

    ServerResponse pay(Long orderNo,int userId,String filePath) throws IOException;

    public ServerResponse alipayCallback(Map<String ,String> params);

    ServerResponse<OrderVO> createOrder(Integer shippingId, Integer userId);
}
