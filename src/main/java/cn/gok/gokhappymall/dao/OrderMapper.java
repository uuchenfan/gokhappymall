package cn.gok.gokhappymall.dao;

import cn.gok.gokhappymall.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Author: chenfan
 * @Description:
 */
public interface OrderMapper extends BaseMapper<Order> {
}
