package cn.gok.gokhappymall.dao;

import cn.gok.gokhappymall.entity.Cart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * @Author: chenfan
 * @Description:
 */
public interface CartMapper extends BaseMapper<Cart> {

    @Update("update t_mall_cart set quantity = #{num} where product_id = #{productId} and user_id = #{userId}")
    int updateCartProductNum(@Param("num") Integer num,@Param("productId") Integer productId,@Param("userId") Integer userId);
}
