package cn.gok.gokhappymall.service.impl;

import cn.gok.gokhappymall.common.ServerResponse;
import cn.gok.gokhappymall.dao.ProductMapper;
import cn.gok.gokhappymall.entity.Product;
import cn.gok.gokhappymall.service.ProductService;
import cn.gok.gokhappymall.util.FTPUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author: chenfan
 * @Description:
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductMapper productMapper;

    @Override
    public ServerResponse<PageInfo<Product>> search(Integer productId, String productName,
                                                    @RequestParam(defaultValue = "1") Integer pageNum,
                                                    @RequestParam(defaultValue = "10") Integer pageSize){

        PageHelper.startPage(pageNum, pageSize);


        QueryWrapper queryWrapper = new QueryWrapper();
        if(productId !=null ){
            queryWrapper.eq("id", productId);
        }
        if(StringUtils.isNotBlank(productName) ){
            queryWrapper.eq("name", productName);
        }



        List<Product> products = productMapper.selectList(queryWrapper);

        PageInfo<Product> pageInfo = new PageInfo<>(products);


        return ServerResponse.createBySuccess(pageInfo);
    }


    /**
     *
     * @param mf     文件对象
     * @param path   项目类路径下的static/upload  文件夹 ，此文件夹作为上传文件的临时文件夹
     */
    @Override
    public ServerResponse upload(MultipartFile mf,String path) throws IOException {

        //1、重新构建文件名，避免文件上传时重复
        //获取项目的原文件名
        String originalFilename = mf.getOriginalFilename();
        //获取文件的后缀名   xzz.png
        String suffix = originalFilename.substring(originalFilename.lastIndexOf(".")+1);
        //产生新的文件名
        String newFilename = UUID.randomUUID().toString() + "." +suffix;

        //2、创建目标文件对象
        File tempTargetFile = new File(path, newFilename);

        //3、将文件上传至临时文件夹中
        mf.transferTo(tempTargetFile);


        //4、调用自定义的工具类将文件上传至ftp服务器
        FTPUtil ftpUtil = new FTPUtil();
        boolean isUploadFile = ftpUtil.uploadFile(tempTargetFile);



        if (isUploadFile){
            Map<String, String> map = new HashMap<>();
            map.put("uri", tempTargetFile.getName());
            map.put("url", "ftp://localhost:21"+File.separator + tempTargetFile.getName());

            //5、将临时文件删除
            tempTargetFile.delete();

           return ServerResponse.createBySuccess(map);
        }
        return ServerResponse.createByErrorMessage("上传失败");
    }

    @Override
    public ServerResponse addProduct(Product product) {
        return null;
    }

    @Override
    public ServerResponse updateProduct(Product product) {
        return null;
    }


}
