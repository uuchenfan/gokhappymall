package cn.gok.gokhappymall.service;

import cn.gok.gokhappymall.common.ServerResponse;
import cn.gok.gokhappymall.entity.Product;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @Author: chenfan
 * @Description:
 */
public interface ProductService {

    ServerResponse<PageInfo<Product>> search(Integer productId, String productName, Integer pageNum, Integer pageSize);

    ServerResponse upload(MultipartFile mf, String path)throws IOException;

    ServerResponse addProduct(Product product);

    ServerResponse updateProduct(Product product);

}


