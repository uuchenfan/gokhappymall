package cn.gok.gokhappymall.service;

import cn.gok.gokhappymall.common.ServerResponse;
import cn.gok.gokhappymall.controller.vo.CartVOList;
import cn.gok.gokhappymall.entity.Cart;

/**
 * @Author: chenfan
 * @Description:
 */
public interface CartService {


    ServerResponse<CartVOList> addCart(Cart cart);
}
