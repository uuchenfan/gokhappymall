package cn.gok.gokhappymall.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author: chenfan
 * @Description:
 */
@Controller
public class TestController {

    @GetMapping("/upload.html")
    public String upload(){
        return "upload.html";
    }

}
