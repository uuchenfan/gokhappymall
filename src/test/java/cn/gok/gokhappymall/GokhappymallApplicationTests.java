package cn.gok.gokhappymall;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

@SpringBootTest
class GokhappymallApplicationTests {

    @Test
    void contextLoads() throws FileNotFoundException {

        //获取到项目路径
        String path = ResourceUtils.getURL("classpath:").getPath();

        //获取到 static/upload
        File upload = new File(path, "static/upload");

        if (!upload.exists()){
            upload.mkdirs();
        }

        System.out.println(path);
    }

}
