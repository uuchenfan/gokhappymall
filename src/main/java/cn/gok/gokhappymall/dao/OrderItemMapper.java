package cn.gok.gokhappymall.dao;

import cn.gok.gokhappymall.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Author: chenfan
 * @Description:
 */
public interface OrderItemMapper extends BaseMapper<OrderItem> {
}
