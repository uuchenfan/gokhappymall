package cn.gok.gokhappymall.service;

import cn.gok.gokhappymall.common.ServerResponse;
import cn.gok.gokhappymall.entity.Category;

import java.util.List;

/**
 * @Author: chenfan
 * @Description:
 */
public interface CategoryService {

    ServerResponse<List<Category>> queryCategoriesByParentCategoryId(Integer parentCategoryId);
    ServerResponse addCategorie(Integer parentCategoryId,String categoryName);

    ServerResponse<List<Integer>> queryCategoryAndChildrenId(Integer categoryId);
}
