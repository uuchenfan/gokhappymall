package cn.gok.gokhappymall.controller;

import cn.gok.gokhappymall.common.ServerResponse;
import cn.gok.gokhappymall.controller.vo.CartVOList;
import cn.gok.gokhappymall.entity.Cart;
import cn.gok.gokhappymall.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: chenfan
 * @Description:
 */
@RestController
@RequestMapping("/cart")
public class CartController {


    @Autowired
    CartService cartService;


    @PostMapping("/add.do")
    public ServerResponse addCart(Integer productId, Integer count){

        Cart cart = new Cart();
        cart.setUserId(1);
        cart.setProductId(productId);
        cart.setQuantity(count);
        cart.setChecked(0);
        return cartService.addCart(cart);
    }
}
