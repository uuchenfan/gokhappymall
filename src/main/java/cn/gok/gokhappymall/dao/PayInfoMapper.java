package cn.gok.gokhappymall.dao;

import cn.gok.gokhappymall.entity.PayInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Author: chenfan
 * @Description:
 */
public interface PayInfoMapper extends BaseMapper<PayInfo> {
}

