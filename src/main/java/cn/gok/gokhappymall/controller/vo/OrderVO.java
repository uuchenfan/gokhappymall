package cn.gok.gokhappymall.controller.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author: chenfan
 * @Description:
 */
@Data
public class OrderVO {
    private Long orderNo;

    private Integer shippingId;

    private BigDecimal payment;

    private Integer paymentType;

    private Integer postage;

    private Integer status;

    private Date paymentTime;

    private Date sendTime;

    private Date endTime;

    private Date closeTime;

    private List<OrderItemVO> orderItemVoList;

    private ShippingVO shippingVO;


}
