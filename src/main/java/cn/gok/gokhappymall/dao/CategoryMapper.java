package cn.gok.gokhappymall.dao;

import cn.gok.gokhappymall.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: chenfan
 * @Description:
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {


}
