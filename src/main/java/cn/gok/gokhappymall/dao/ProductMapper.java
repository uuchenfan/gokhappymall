package cn.gok.gokhappymall.dao;

import cn.gok.gokhappymall.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: chenfan
 * @Description:
 */
@Mapper
public interface ProductMapper extends BaseMapper<Product> {

}
