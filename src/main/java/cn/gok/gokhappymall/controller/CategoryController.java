package cn.gok.gokhappymall.controller;

import cn.gok.gokhappymall.common.ServerResponse;
import cn.gok.gokhappymall.entity.Category;
import cn.gok.gokhappymall.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: chenfan
 * @Description:
 */
@RestController
@RequestMapping(value ="manage")
public class CategoryController {


    @Autowired
    CategoryService service;

    @GetMapping("/category/get_category.do")
    public ServerResponse<List<Category>> queryCategores(@RequestParam(defaultValue = "0") Integer categoryId){

        ServerResponse<List<Category>> listServerResponse = service.queryCategoriesByParentCategoryId(categoryId);
        return listServerResponse;
    }



    @PostMapping("/category/add_category.do")
    public ServerResponse addCategore(@RequestParam(defaultValue = "0") Integer parentId,String categoryName){
        ServerResponse serverResponse = service.addCategorie(parentId, categoryName);
        return serverResponse;
    }


    @GetMapping("/category/get_deep_category.do")
    public ServerResponse<List<Integer>> geDeepCcategory(Integer categoryId){
        ServerResponse<List<Integer>> childrenIds = service.queryCategoryAndChildrenId(categoryId);
        return childrenIds;
    }



}
